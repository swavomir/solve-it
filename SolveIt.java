import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.IllegalArgumentException;

public class SolveIt {

	public static void main(String[] args) throws Exception {
		p("enter a number to calculate square root, or hit <Enter> key to exit");

		BufferedReader bufferedReader = new BufferedReader(
			new InputStreamReader(System.in));

		SolveIt s = new SolveIt();

		String input;
		while ((input = bufferedReader.readLine()).length() > 0) {
			p(String.format("you've entered: '%s'", input));

			try {
				/*
				double d = s.toDouble(input);
				p(String.format("calculating square root of '%.2f'", d));
				*/
				int i = s.toInt(input);
				double sqrRoot = s.sqrRoot(i);
				p(String.format("square root of %d is %.6f", i, sqrRoot));

			} catch (IllegalArgumentException ex) {
				p(String.format("%s is not a number, enter another number or <Enter> key to exit", input));
			}
		};

		p("exiting...");

	}

	final static int ZERO = (int)'0';
	final static double EPS = 0.001;

	double sqrRoot(int val) {
		//start with 1/4 of val
		return sqrRootAux(val, val / 4.0);
	}

	double sqrRootAux(int val, double guess) {
		p(String.format("guess=%.3f", guess));	

		double check = guess * guess;

		if (Math.abs(val - check) <= EPS) {
			return guess;
		} else {
			double reciprocal = val / guess;
			return sqrRootAux(val, (reciprocal + guess)/2);
		}
	}

	//TODO: double toDouble(String s) {
	int toInt(String s) {
		String s1 = s.trim();

		int result = toInt(s1, 0, 0);

		return result;
	}

	int toInt(String s1, int idx, int accum) {
		if (idx == s1.length()) 
			return accum;
		else {
			int asciiVal = (int)s1.charAt(idx);
			if (asciiVal >= 48 && asciiVal <= 57) {
				p(String.format("char at %d is %c ascii val: %d", idx, s1.charAt(idx), asciiVal));
				return toInt(s1, idx+1, accum * 10 + asciiVal - ZERO);
			} else {
				throw new IllegalArgumentException(s1 + " is not a number");
			}
		}
	}

	static void p(String s) {System.out.println(s);}

}